import React,{useState,useEffect} from 'react'

import {Row,Col,Card,Button} from 'react-bootstrap'

export default  function Highlights (){


	const [productsArray,setProductsArray] = useState([])



	useEffect(()=>{

		fetch("https://desolate-forest-57391.herokuapp.com/products/activeProducts")
		.then(res => res.json())
		.then(data =>{

			
			setProductsArray(data.map(product => {
				console.log(product)
					return (
				<>
					<Card className="col-md-4 m-3">
						<Card.Img variant="top" src={product.fileInput} />
							<Card.Body>
								<Card.Title><strong>{product._id}</strong></Card.Title>
									<Card.Text><mark className ="bg-dark text-light">
										Name : {product.name}
									</mark></Card.Text>
									<Card.Text><mark className ="bg-dark text-light">
										Price : {product.price}
									</mark></Card.Text>
								<Button  variant="primary">View Item</Button>	
						</Card.Body>
					</Card>
				</>
		)
				
			}))
		})
	},[])

	return (
			<>
				<h1 className="my-5 bg-dark text-light d-inline offset-4 col-md-4 cstyle">Available Products</h1>
				<div className = "d-flex flex-wrap justify-content-around">
				{productsArray}
				</div>
			</>
		)
}
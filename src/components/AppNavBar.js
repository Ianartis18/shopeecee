import React,{useContext} from 'react';
import {Navbar,Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../userContext'


export default function AppNavBar(){

	const {user} = useContext(UserContext)

	return(

		<Navbar bg="primary" expand = "lg">
			
			<Navbar.Brand as={Link} to="/"><i class="fab fa-stripe-s"></i>hoPeeCee</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to="/"><i class="fas fa-house-user"></i></Nav.Link>
					<Nav.Link as={Link} to="/products">Products</Nav.Link>
					{
						user.id
						?
							user.isAdmin
							?
							<>
							<Nav.Link as={Link} to="/addProducts">Add Products</Nav.Link>
							<Nav.Link as={Link} to="/logout"><i class="fas fa-sign-out-alt"></i></Nav.Link>
							</>
							:
							<>
							<Nav.Link as={Link} to="/logout"><i class="fas fa-sign-out-alt"></i></Nav.Link>
							</>
						:
						<>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}
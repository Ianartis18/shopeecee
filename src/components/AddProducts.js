import React,{useState,useEffect,useContext} from 'react'

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';

import {Redirect,useHistory} from 'react-router-dom'

import UserContext from '../userContext'




export default function AddProduct(){

	const {user} = useContext(UserContext)

	const history = useHistory();

	const [name,setName] = useState("")

	const [description,setDescription] = useState("")

	const [fileInput,setFileInput] = useState("")

	const [price,setPrice] = useState(0)

	const [isActive,setIsActive] = useState(true)



	function reset(){
		setName("")
		setDescription("")
		setPrice("")
	}

	useEffect(()=>{

		if(name !== "" && description !== "" && price !== 0 && fileInput !== ""){

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	},[name, description, price, fileInput]) 
	
	function addProduct(e){

		e.preventDefault()
		fetch('https://desolate-forest-57391.herokuapp.com/products/addProducts',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({

				name: name,
				description: description,
				price: price,
				fileInput:fileInput
			})
		})
		.then(res => res.json())
		.then(data => {

			
			if(data.message){
				Swal.fire({

					icon: "error",
					title: "Adding Products Failed.",
					text: data.message
				})
			} else {
				console.log(data)
				Swal.fire({

					icon: "success",
					title: "Adding Products Successful.",
					text: `New Product Added.`
				})
				history.push("/products")
				console.log(history)
			}

		})
		reset()	
	}

	
	return (
		user.isAdmin === false 
		?
		<Redirect to="/"/>
		:
		<div>
				<h1 className="text-center bg-dark text-light px-2 py-1 offset-4 col-3">Add Product</h1>
				<Form onSubmit={ e => addProduct(e)}>
					<Form.Group controlId="name">
						<Form.Label className="bg-dark text-light px-2 py-1">Product Name:</Form.Label>
						<Form.Control type= "text" value ={name} onChange={e => {setName(e.target.value)}} placeholder = "Enter Name" required/>
					</Form.Group>
					<Form.Group controlId="description">
						<Form.Label className="bg-dark text-light px-2 py-1">Description:</Form.Label>
						<Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)} placeholder = "Enter Description"required/>
					</Form.Group>
					<Form.Group controlId="price">
						<Form.Label className="bg-dark text-light px-2 py-1">Price:</Form.Label>
						<Form.Control type="number" value={price} onChange={(e) => setPrice(e.target.value)} placeholder = "Enter Price"required/>
					</Form.Group>
					<Form.Group controlId="fileInput">
					    <Form.Label className="bg-dark text-light px-2 py-1">Attach File:</Form.Label>
					    <Form.Control type="text" value={fileInput} onChange={(e) => setFileInput(e.target.value)} placeholder = "IMG URL" required/>
					</Form.Group>
					{
						isActive 
						? <Button type="submit" variant="primary">Submit</Button>
						: <Button type="submit" variant="primary" disabled>Submit</Button>
					}
				</Form>
		</div>
		

		)

}

import React from 'react';
import {Row,Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function Banner ({bannerProp}) {

	return (

			<Row>
				<Col>
					<div className ="text-center my-5 ">
					<h1 className=" bg-dark text-light d-block offset-4 col-md-4 my-2 cstyle">{bannerProp.title}</h1>
					<h1 className="my-2 bg-dark text-light d-block offset-4 col-md-4 cstyle">{bannerProp.description}</h1>
					<Link to ={bannerProp.destination} className="btn btn-primary my-2">{bannerProp.buttonCallToAction}</Link>
					</div>
				</Col>
			</Row>


		)
}
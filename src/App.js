import React,{useState,useEffect} from 'react';
import AppNavBar from './components/AppNavBar';
import { BrowserRouter as Router } from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';
import {UserProvider} from './userContext'
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import Logout from  './pages/Logout';
import ViewProduct from  './pages/ViewProduct';
import AddProducts from  './components/AddProducts';
import UpdateProduct from  './components/UpdateProduct';
import {Container} from 'react-bootstrap';
import './App.css'

export default function App() {

  const [user,setUser] = useState({

    id: null,
    isAdmin: null

  })

  useEffect(()=>{

    fetch('https://desolate-forest-57391.herokuapp.com/users/userDetails',{

      headers: {

        'Authorization': `Bearer ${localStorage.getItem('token')}`

      }

    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })

    })

  },[])

  console.log(user);

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
      <>  
        <UserProvider value={{user,setUser,unsetUser}}>
          <Router>
            <AppNavBar />
                <Container>
                  <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/products" component={Products}/>
                    <Route exact path="/login" component={Login}/>
                    <Route exact path="/register" component={Register}/>
                    <Route exact path="/logout" component={Logout}/>
                    <Route exact path="/addProducts" component={AddProducts}/>
                    <Route exact path="/updateProducts" component={UpdateProduct}/>
                    <Route exact path="/orders" component={ViewProduct}/>

                    <Route component={NotFound}/>
                  </Switch>
                </Container>
          </Router>
        </UserProvider>
      </>
    )

}
import React,{useState,useEffect,useContext} from 'react'

import {Form,Button} from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect,Link} from 'react-router-dom'

import Fade from 'react-reveal/Fade';






export default function Login(){

	const{user,setUser} = useContext(UserContext)
	const[email,setEmail] = useState("")
	const[password,setPassword] = useState("")
	const[isActive,setIsActive] = useState(false)

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[email,password])

	function loginUser(e){
		e.preventDefault()

		fetch('https://desolate-forest-57391.herokuapp.com/users/login',{

			method :'POST',
			headers:{
				"Content-Type":"application/json"
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data.accessToken){
					Swal.fire({

						icon:"success",
						title:"Login Successful!",
						text:`Thank you for logging in`
					})
					localStorage.setItem('token',data.accessToken)
					
					fetch('https://desolate-forest-57391.herokuapp.com/users/userDetails',{

						headers:{
								'Authorization': `Bearer ${localStorage.getItem('token')}`
								}
							})
							.then(res => res.json())
							.then(data => {
								setUser({
									id:data._id,
									isAdmin:data.isAdmin
								})
							})
						
				} else {
					Swal.fire({
						icon:"error",
						title:"Login Failed",
						text: data.message
					})
				}

		})
	}

	return (
		user.id
		?
		<Redirect to="/products"/>
		:
		<>
			<Fade top>
			<h1 className ="my-5 text-center my-5 text-center bg-dark text-light offset-5 col-2 px-2">Login</h1>
			<Form onSubmit = {e=>loginUser(e)}>
				<Form.Group>
					<Form.Label className="text-center bg-dark text-light px-2 py-1">Email: </Form.Label>
					<Form.Control type= "email" value ={email} onChange={e => {setEmail(e.target.value)}} placeholder = "Enter Email" required/>
				</Form.Group> 
				<Form.Group>
					<Form.Label className="text-center bg-dark text-light px-2 py-1">Password </Form.Label>
					<Form.Control type= "password" value ={password} onChange={e => {setPassword(e.target.value)}} placeholder = "Enter Password" required/>
				</Form.Group>
				{
					isActive
					?<Button variant="primary" type="submit">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
			<h4 className="text-center bg-dark text-light px-2 py-1 col-4">Don't have an account? <Link to ="/register">Sign up</Link></h4>
			</Fade>

		</>
		)
}
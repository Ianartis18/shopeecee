import React from 'react';

import Banner from '../components/Banner'

import Highlights from '../components/Highlights'

export default function Home(){

	let sampleProp = {
	title:"ShoPeeCee",
	description:"EVERY GAMER NEEDS!!",	
	buttonCallToAction: "Shop now",
	destination:"/login"}

	return(
	
			<div>
				
				<Banner bannerProp ={sampleProp}/>
				<Highlights/>
			
			</div>

		)
}
import React,{useState,useEffect,useContext} from 'react'

import {Form,Button} from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect,useHistory} from 'react-router-dom'

import Fade from 'react-reveal/Fade';





export default function Register(){

	const{user} = useContext(UserContext)
	const history = useHistory();
	//input
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")
	//conditional rendering
	const [isActive,setIsActive] = useState(false)

	

	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (mobileNo.length ===11)&&(password === confirmPassword)){

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	function registerUser(e){

		e.preventDefault()
	console.log(firstName)
	console.log(lastName)
	console.log(email)
	console.log(mobileNo)
	console.log(password)
	console.log(confirmPassword)

			fetch('https://desolate-forest-57391.herokuapp.com/users/register',{

				method :'POST',
				headers:{
					"Content-Type":"application/json"
				},
				body:JSON.stringify({
					firstName: firstName,
					lastName:lastName,
					email:email,
					mobileNo:mobileNo,
					password:password,
					confirmPassword:confirmPassword

				})
			})
			.then(res=>res.json())
			.then(data =>{
				console.log(data)
				if(data.email){
					Swal.fire({

						icon:"success",
						title:"Registration Successful!",
						text:`Thank you for registering, ${data.email}`
					})
					history.push('/login')
				} else {
					Swal.fire({
						icon:"error",
						title:"Registration Failed",
						text: data.message
					})
				}
			})
	}

	return (
		user.id
		?
		<Redirect to="/products"/>
		:
		<>
			<Fade bottom>
			<h1 className ="my-5 text-center bg-dark text-light offset-5 col-2">Register</h1>
			<Form onSubmit = {e=>registerUser(e)}>
				<Form.Group>
					<Form.Label className="bg-dark text-light px-2 py-1">First Name: </Form.Label>
					<Form.Control type= "text" value ={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder = "Enter First Name" required/>
				</Form.Group> 
				<Form.Group>
					<Form.Label className="bg-dark text-light px-2 py-1">Last Name: </Form.Label>
					<Form.Control type= "text" value ={lastName} onChange={e => {setLastName(e.target.value)}} placeholder = "Enter Last Name" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="bg-dark text-light px-2 py-1">Email: </Form.Label>
					<Form.Control type= "email" value ={email} onChange={e => {setEmail(e.target.value)}} placeholder = "Enter Email" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="bg-dark text-light px-2 py-1">Mobile No: </Form.Label>
					<Form.Control type= "number" value ={mobileNo} onChange={e => {setMobileNo(e.target.value)}} placeholder = "Enter 11 Digit Mobile No" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="bg-dark text-light px-2 py-1">Password: </Form.Label>
					<Form.Control type= "password" value ={password} onChange={e => {setPassword(e.target.value)}} placeholder = "Enter Password" required/>
				</Form.Group>
				<Form.Group>
					<Form.Label className="bg-dark text-light px-2 py-1">Confirm Password: </Form.Label>
					<Form.Control type= "password" value ={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} placeholder = "Confirm Password" required/>
				</Form.Group> 
				{
					isActive
					?<Button variant="primary" type="submit">Submit</Button>
					:<Button variant="primary" disabled>Submit</Button>
				}
			</Form>
			</Fade>
		</>


		)

}
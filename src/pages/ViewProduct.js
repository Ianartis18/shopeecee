import React,{useState,useEffect,useContext} from 'react'
import {Card,Button,Row,Col} from 'react-bootstrap'
import {useParams,useHistory,Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../userContext'

export default function ViewProduct(){


	const {productId} = useParams();

	const{user} = useContext(UserContext);

	const history = useHistory()
	
	const [productDetails,setProductDetails] = useState({
	
		name:null,
		description:null, 
		price:null,
		fileInput:null
	})
	console.log(productId)
	useEffect(()=>{
		fetch(`https://desolate-forest-57391.herokuapp.com/products/singleProducts/${productId}`)
		.then(res => res.json())
		.then(data => {
			
			if(data.message){
				Swal.fire({
					icon:"error",
					title:"Product Unavailable",
					text: data.message
				})
			} else {
				setProductDetails({
					name:data.name,
					description:data.description,
					price:data.price,
				})
			}
		})
	},[productId])
	console.log(productDetails)

	function order (productId){

			fetch(`https://desolate-forest-57391.herokuapp.com/users/orders`,{
				method: 'POST',
				headers: {
					'Content-Type':'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body:JSON.stringify({
					productId:productId
				})	
			})
			.then(res => res.json())
			.then(data => {

				if(data.message === "Enrolled Successfully."){
					Swal.fire ({
						icon:'success',
						title: 'Successfully Enrolled',
						text: "You have successfully enrolled for this course."
					})
					history.push("/products")
				}else
					Swal.fire({
						icon:"error",
						title:"Enrollment Failed",
						text:"Something went wrong",

					})
			})
		
	}

	return(
		 
			<Row className ="mt-5">
				<Col>
					<Card>
						<Card.Body className ="text-center">
							<Card.Title>{productDetails.name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{productDetails.description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{productDetails.price}</Card.Text>
							<Card.Subtitle>Schedule:</Card.Subtitle>
							{
								user.isAdmin === false
								?
								<Button variant="primary" block onClick={()=>order(productId)}>Add to Cart</Button>
								:
								<Link  className ="btn btn-danger btn-black" to ="/login"> Login to Enroll</Link>

							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)
}


import React from 'react'
import Banner from '../components/Banner'

export default function NotFound({bannerProp}){

	let bannerContent = { 
		title:"Page Not Found",
		description:"Buy the thing you need",
		buttonCallToAction: "click here!",
		destination:"/"
	}
	return(
			
				<Banner bannerProp ={bannerContent} />
			
		)


}
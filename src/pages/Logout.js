import React,{useContext,useEffect} from 'react'

import UserContext from '../userContext'

import Banner from '../components/Banner'







export default function Logout(){

	const {setUser,unsetUser} = useContext(UserContext)

	unsetUser();

	
	useEffect(()=>{

		setUser({
			id:null,
			isAdmin:null
		})
	},[])


	const bannerContent = {
		title: "You are logged out",
		description : "Pleasure doing business with you",
		buttonCallToAction: "Go Back to Home Page.",
		destination:"/",
	}

	return (

			<Banner bannerProp={bannerContent}/>
		)
}
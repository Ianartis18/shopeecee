import React,{useState,useEffect,useContext} from 'react'

import Product from '../components/Product'
	
import UserContext from '../userContext'

import {Table,Button} from 'react-bootstrap'

import {Link} from 'react-router-dom'


export default function Products(){


	const{user} = useContext(UserContext)
	console.log(user)

	const [productsArray,setProductsArray] = useState([])

	const [allProducts,setAllProducts] = useState([])

	const [update,setUpdate] = useState(0)


	useEffect(()=>{

		fetch("https://desolate-forest-57391.herokuapp.com/products/activeProducts")
		.then(res => res.json())
		.then(data =>{

			
			setProductsArray(data.map(product => {

					return (
							<>
								<Product key ={product._id} productProp ={product}/>
							</>
						)
				
			}))
		})
	},[])

	useEffect(()=>{

		if(user.isAdmin){

			fetch('https://desolate-forest-57391.herokuapp.com/products/',{

				headers:{
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}

			})
			.then(res => res.json())
			.then(data => {

				setAllProducts(data.map((product) =>{

					return(

							<tr key={product.id}>
								<td>{product._id}</td>
								<td>{product.name}</td>
								<td>{product.price}</td>
								<td>{product.isActive ? "Active":"Inactive"}</td>
								<td>
									{
										product.isActive
										?
										<div className ="text-center">	
											<span className ="font-weight-bold">Archive</span>
											<Button variant="danger" className = "mx-2" onClick={()=>{
											archive(product._id)}}><i class="fas fa-toggle-off"></i></Button>
										</div>
										:
										<div className ="text-center">	
											<span className ="font-weight-bold">Activate</span>
											<Button variant="success" className = "mx-2" onClick={()=>{
											activate(product._id)}}><i class="fas fa-toggle-on"></i></Button>
										</div>
									}
								</td>
								<td>
								<div>
									<span className ="font-weight-bold pl-2">Edit</span>
									<Button as={Link} to="/updateProducts" variant="dark" className = "mx-2" onClick={()=>{
									editProduct(product._id)} }><i class="far fa-edit"></i></Button>
								</div>
								</td>
							</tr>

						)
				}))
			})
		}
	},[user,update])

	function archive(productsId){
		console.log(productsId)
		
		fetch(`https://desolate-forest-57391.herokuapp.com/products/archiveProducts/${productsId}`,{

			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setUpdate(update+1)

		})

	}
	function activate(productsId){
		console.log(productsId)
		
		fetch(`https://desolate-forest-57391.herokuapp.com/products/activateProducts/${productsId}`,{

			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setUpdate(update+1)

		})

	}
	function editProduct(productsId) {
		
		localStorage.setItem('productsId',productsId)

			
	}


	return(
		user.isAdmin
		?
		<>
		 	<h1 className ="text-center my-5">Admin Dashboard</h1>
		 	<Table striped bordered hover className ="table-light table-responsive-md">
		 		<thead className ="thead-dark">
		 			<tr>
		 				<th>ID</th>
		 				<th>Name</th>
		 				<th>Price</th>
		 				<th>Status</th>
		 				<th>Actions</th>
		 				<th>Update</th>
		 			</tr>
		 		</thead>
		 		<tbody>
		 			{allProducts}
		 		</tbody>
		 	</Table>
		</>
		:
			<>
				
				<h1 className="my-5 bg-dark text-light flex text-center cstyle">Available Products</h1>
				<div className = "flex-sm-column justify-content-sm-center">
				{productsArray}
				</div>
			</>
		)	
}